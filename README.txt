CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Installation

INTRODUCTION
------------

Provides integration with jQuery Biggerlink plugin
(http://www.ollicle.com/projects/jquery/biggerlink/) to make it
really easy to enable the specified element's to behave as a proxy for their
first contained link.

INSTALLATION
------------

1. Create a directory named biggerlinkg' under your site's libraries
   directory (either sites/all/libraries or sites/SITENAME/libraries).

2. Download the jquery.biggerlink.min.js from
   http://www.ollicle.com/projects/jquery/biggerlink/ and put it in this
   directory.

4. Enable the jQuery Biggerlink module as usual. If installed correctly,
   you should see the library version number at admin/reports/status.

5. Use it in your code like this:
   libraries_load('biggerlink');
   $path = drupal_get_path('module', 'YOURMODULENAME');
   drupal_add_js($path .'/init-biggerlink.js', array('scope' => 'footer'));

   Where init-biggerlink.js contains:
   (function($) {
     $('.some-html-class li').biggerlink();
   })(jQuery);
